#lang racket
;;; down :: list? ­> list? 
(define (down lst)
    (map (lambda(s)(string-append "(" s)) 
        (map (lambda(s)(string-append s ")")) 
            (map (lambda(s)(if (not (string? s)) (number->string s) s ) ) 
             lst )
        )
    )
)

